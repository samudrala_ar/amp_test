/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CampaignBatch implements Database.AllowsCallouts, Database.Batchable<SObject> {
    global Boolean runNextJobInChain;
    global CampaignBatch() {

    }
    global CampaignBatch(Boolean runNextJobInChain) {

    }
    global void execute(Database.Batchable<sObject>Context context, List<SObject> records) {

    }
    global void finish(Database.Batchable<sObject>Context context) {

    }
    global Database.QueryLocator start(Database.Batchable<sObject>Context context) {
        return null;
    }
}
