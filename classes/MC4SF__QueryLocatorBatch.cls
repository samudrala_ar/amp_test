/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class QueryLocatorBatch implements Database.AllowsCallouts, Database.Batchable<SObject>, Database.Stateful {
    global MC4SF.QueryLocatorBatch.Implementation impl;
    global void execute(Database.Batchable<sObject>Context context, List<SObject> records) {

    }
    global void finish(Database.Batchable<sObject>Context context) {

    }
    global Database.QueryLocator start(Database.Batchable<sObject>Context context) {
        return null;
    }
}
